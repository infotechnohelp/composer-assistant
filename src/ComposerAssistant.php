<?php

namespace ComposerAssistant;

/**
 * Class ComposerAssistant
 * @package ComposerAssistant
 */
class ComposerAssistant
{
    /**
     * @var null|string
     */
    private $namespace = null;

    /**
     * ComposerAssistant constructor.
     * @param string $namespace
     */
    public function __construct(string $namespace = 'infotechnohelp')
    {
        $this->namespace = $namespace;
    }

    /**
     * @param string $msg
     */
    public function printOut(string $msg)
    {
        echo sprintf("\n\n→   %s\n\n\n\n", $msg);
    }

    /**
     * @param string $commitString
     * @return array
     */
    public function parseCommitInput(string $commitString)
    {
        $packageTitle = null;
        $commitMessage = null;
        $commitHash = null;

        if (strpos($commitString, ':') !== false) {
            list($packageTitle, $commitMessage) = explode(':', $commitString);
        }

        if (strpos($commitString, '#') !== false) {
            list($packageTitle, $commitHash) = explode('#', $commitString);
        }

        return [$packageTitle, $commitMessage, $commitHash];
    }

    /**
     * @param $packageTitle
     * @param $commitMessage
     * @param null $parentPackageTitle
     * @param array $check
     * @param string|null $hash
     * @return mixed|string
     */
    public function process($packageTitle, $commitMessage, $parentPackageTitle = null, $check = [], string $hash = null)
    {

        if ($hash === null) {
            $hash = $this->commit($packageTitle, $commitMessage);

            $this->push($packageTitle);
        }

        $restarts = 0;

        while ($restarts < 3 && !$this->update($packageTitle, $hash, $parentPackageTitle, $check)) {
            $restarts++;
        }

        if ($restarts === 3) {
            $this->printOut("RESTARTED 3 TIMES WITHOUT SUCCESS: " . json_encode($check));
            die();
        }


        return $hash;
    }

    /**
     * @param string $commitMessage
     * @return mixed
     */
    public function commitSelf(string $commitMessage)
    {
        $commands = [
            "git add . & git commit -a -m \"$commitMessage\"",
        ];

        $this->executeCommands($commands);

        $output = [];
        $command = "git rev-parse HEAD";
        exec($command . ' 2>&1', $output);

        return $output[0];
    }


    public function pushSelf()
    {
        $commands = [
            "git push",
        ];

        $this->executeCommands($commands);
    }

    /**
     * @param array $commands
     * @param bool $instantOutput
     */
    private function executeCommands(array $commands, bool $instantOutput = true)
    {
        foreach ($commands as $command) {

            $this->printOut("$ $command");

            if ($instantOutput) {
                exec($command);
                continue;
            }

            $output = [];
            exec($command . ' 2>&1', $output);

            foreach ($output as $line) {
                echo "$line\n";
            }
        }
    }

    /**
     * @param $packageTitle
     * @param $parentPackageTitle
     */
    private function remove($packageTitle, $parentPackageTitle)
    {
        $commands[] = ($parentPackageTitle === null) ?
            sprintf("composer remove %s/%s", $this->namespace, $packageTitle) :
            sprintf("cd vendor/%s/%s & composer remove %s/%s",
                $this->namespace, $parentPackageTitle, $this->namespace, $packageTitle);

        $this->executeCommands($commands);
    }

    /**
     * @param $packageTitle
     * @param $hash
     * @param null $parentPackageTitle
     * @param array $check
     * @return bool
     */
    private function requireMasterBranch($packageTitle, $hash, $parentPackageTitle = null, array $check = [])
    {
        $command = ($parentPackageTitle === null) ?
            sprintf("composer require %s/%s:dev-master#%s", $this->namespace, $packageTitle, $hash) :
            sprintf("cd vendor/%s/%s & composer require %s/%s:dev-master#%s",
                $this->namespace, $parentPackageTitle, $this->namespace, $packageTitle, $hash);

        if ($check === []) {
            $this->printOut("$ $command");
            exec($command);
            return true;
        }

        $this->printOut("$ $command\n\nLOADING...");

        $output = [];
        exec($command . ' 2>&1', $output);

        $errorData = [];

        foreach ($output as $line) {
            echo "$line\n";
            foreach ($check as $checkedPackage => $requiredHash) {
                if (strpos($line, $checkedPackage) !== false && strpos($line, substr($requiredHash, 0, 7)) === false) {
                    $errorData[$checkedPackage] = $requiredHash;
                }
            }
        }

        if (count($errorData) > 0) {
            $this->printOut("WARNING: restart required for " . json_encode($errorData));
            return false;
        }

        return true;
    }

    /**
     * @param $packageTitle
     * @param $hash
     * @param null $parentPackageTitle
     * @param array $check
     * @return bool
     */
    private function update($packageTitle, $hash, $parentPackageTitle = null, array $check = [])
    {
        $this->remove($packageTitle, $parentPackageTitle);

        return $this->requireMasterBranch($packageTitle, $hash, $parentPackageTitle, $check);
    }

    /**
     * @param string $packageTitle
     * @param string $commitMessage
     * @return mixed
     */
    private function commit(string $packageTitle, string $commitMessage)
    {

        $commands = [
            sprintf("cd vendor/%s/%s & git add . & git commit -a -m \"%s\"",
                $this->namespace, $packageTitle, $commitMessage),
        ];

        $this->executeCommands($commands, false);

        $output = [];
        $command = sprintf("cd vendor/%s/%s & git rev-parse HEAD", $this->namespace, $packageTitle);
        exec($command . ' 2>&1', $output);

        $this->printOut("$packageTitle: " . $output[0]);

        return $output[0];
    }

    /**
     * @param string $packageTitle
     */
    private function push(string $packageTitle)
    {
        $command = sprintf("cd vendor/%s/%s & git push",
            $this->namespace, $packageTitle);

        $this->printOut("$ $command\n\nLOADING...");

        $output = [];
        exec($command . ' 2>&1', $output);

        $error = false;
        foreach ($output as $line) {
            echo "$line\n";
            if (strpos($line, "error: failed to push some refs") !== false) {
                $error = true;
            }
        }

        if ($error) {
            $this->printOut("WARNING: $packageTitle could not be pushed");
            die();
        }

    }
}
