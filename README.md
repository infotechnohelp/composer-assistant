## Implementation

Terminal: `composer require infotechnohelp/composer-assistant`

`composer.config.php`

``` 
<?php

return [
    'JsonApi updated',
    [
        'cakephp-core_advanced:JsonApi updated' => [
            'cakephp-json-api_advanced#9c9c6fc196f82b9d43199f4beb4d5c520efbce85',
            ...,
        ],
        ...,
    ],
];
```

`composer.php`

``` 
<?php

require_once('vendor/autoload.php');

list($mainCommitMessage, $inputs) = require_once("composer.config.php");

$composer = new \ComposerAssistant\ComposerAssistant();

foreach ($inputs as $key => $input) {
    if (is_array($input)) {

        $check = [];

        foreach ($input as $innerInput) {
            list($packageTitle, $commitMessage, $hash) = $composer->parseCommitInput($innerInput);
            list($keyPackageTitle, $keyCommitMessage, $keyHash) = $composer->parseCommitInput($key);

            $check[$packageTitle] = $composer->process($packageTitle, $commitMessage, $keyPackageTitle, [], $hash);
        }


        list($packageTitle, $commitMessage, $hash) = $composer->parseCommitInput($key);
        $composer->process($packageTitle, $commitMessage, null, $check, $hash);

        continue;
    }

    list($packageTitle, $commitMessage, $hash) = $composer->parseCommitInput($input);
    $composer->process($packageTitle, $commitMessage, null, [], $hash);
}


$composer->printOut("Hash: " . $composer->commitSelf($mainCommitMessage));

$composer->pushSelf();

$composer->printOut("SUCCESS");
```

Terminal: `php composer.php`